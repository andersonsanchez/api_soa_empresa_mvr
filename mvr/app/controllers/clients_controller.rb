class ClientsController < ApplicationController

  protect_from_forgery with: :null_session

  def index
    @clients = Client.all
    render json: @clients.any? ? @clients : {error: "No records found"}
  end

  def show
    @client = Client.find_by(id: params[:id])
    render json: @client ? @client : {error: "Not record found"}
  end

  def create
    begin
      @client = Client.create(client_params)
      render json: @client
    rescue ActiveModel::ForbiddenAttributesError => e
      render json: {error: e}
    rescue ActiveRecord::RecordNotCreated => e
      render json: {error: e}
    end
  end


#def destroy
 # @client = Client.find_by(id: params[:id])
  #render json: @client.nil? ? {error: 'Record not found'} : @client.destroy ? @client : { error: 'Could not be deleted'}
#end

  private

  def client_params
    params.require(:client).permit(:name, :lastname, :ci, :mobile, :phone)
  end

end
