class PathsController < ApplicationController
  protect_from_forgery with: :null_session

  def create
    @shipping = Shipping.find_by(id: paths_params[:shipping_id])
    if @shipping
      @office = Office.find_by(id: paths_params[:office_id])
      if @office
        @path = Path.new
        @path.date = DateTime.now
        @path.arrival_date = ""
        @path.price = paths_params[:price]
        @path.status = "current"
        @path.office_id = @office.id
        @path.shipping_id = @shipping.id
        @shipping.status = "pending"
        @path.save
        @shipping.save
        render json: @path
        @oldpath = Path.find_by(id: @path.shipping_id)
        puts @oldpath.status
        @oldpath.status = "deflected"
        @oldpath.save
      else
        render json: {errors: "Office not found"}
      end
    else
      render json: {errors: "Shipping not found"}
    end
  end

  def update
    @path = Path.find_by(id: params[:id])
    render json: @path.nil? ? {error: 'Record not found'} : @path.update(paths_params)? @path : {error: 'Record could not be updated'}
  end

  private

  def paths_params
    params.require(:path).permit(:arrival_date, :price, :status, :office_id, :shipping_id)
  end
end
