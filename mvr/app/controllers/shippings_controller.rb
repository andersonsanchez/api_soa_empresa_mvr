class ShippingsController < ApplicationController
	include CreateShippingFreightPath
	protect_from_forgery with: :null_session


		def index
			if params[:status]
      	@shipping = Shipping.where('status = ?', params[:status])
				render json: @shipping ? @shipping : {errors: "No shipments found"}
			else
				@shipping = Shipping.all
				render json: @shipping ? @shipping : { errors: "No shipments found"}
			end
  end

		def show
			@shipping = Shipping.find_by(id: params[:id])
			if @shipping
				render json: @shipping, serializer: StatusShippingSerializer
			else
				render json: {error: "Not record found"}
			end
		end

		def create
			create_all_three
		end

		def update
			@shipping = Shipping.find_by(id: params[:id])
			if @shipping.nil?
				render json: { error: 'Record not found'}
			else
					@shipping.pickup_date = DateTime.now
					@shipping.status = shipping_params[:status]
					if @shipping.save
					render json: @shipping, serializer:StatusShippingSerializer
					else
					{error: 'Record could not be updated'}
				end
			end
		end

# def update
# 	@shipping = Shipping.find_by(id: params[:id])
# 	render json: @shipping.nil? ? { error: 'Record not found'} : @shipping.update(shipping_params)?  @shipping : {error: 'Record could not be updated'}
# end

		private

		def shipping_params
			params.require(:shipping).permit(:order_date, :base_price, :pickup_date, :status, :sender_id, :receiver_id, :office_id)
		end
end

