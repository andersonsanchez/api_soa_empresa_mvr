class Freight < ApplicationRecord
  belongs_to :shipping
  enum kind: [:package, :document]
end
