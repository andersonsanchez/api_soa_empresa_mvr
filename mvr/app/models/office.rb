class Office < ApplicationRecord
  belongs_to :state, foreign_key: :state_id, class_name: "Location"
  belongs_to :city, foreign_key: :city_id, class_name: "Location"
  has_many :origin_rates, class_name: "Rate", foreign_key: :origin_id
  has_many :destination_rates, class_name: "Rate", foreign_key: :destination_id
  has_many :paths
end
