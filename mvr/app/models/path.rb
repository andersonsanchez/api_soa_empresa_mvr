class Path < ApplicationRecord
  belongs_to :office
  belongs_to :shipping
  enum status: [:current, :deflected]
end
