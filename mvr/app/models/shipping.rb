class Shipping < ApplicationRecord
  has_many :freights
  has_many :paths
  belongs_to :sender, foreign_key: :sender_id, class_name: "Client"
  belongs_to :receiver, foreign_key: :receiver_id, class_name: "Client"
  belongs_to :office
  enum status: [:pending, :sent, :received, :delivered]
end
