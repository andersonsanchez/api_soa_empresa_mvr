class PathSerializer < ActiveModel::Serializer
  attributes :arrival_date, :price, :status, :office_id, :shipping_id
end
