class ShippingSerializer < ActiveModel::Serializer
  attributes :id, :order_date, :status, :pickup_date

 has_one :sender
 has_one :receiver
 has_one :office
 has_one :freights
 has_many :paths
end
