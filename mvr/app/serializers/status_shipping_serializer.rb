class StatusShippingSerializer < ActiveModel::Serializer
  attributes :order_date, :base_price, :pickup_date, :status, :sender_id, :receiver_id, :office_id
end
