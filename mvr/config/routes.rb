Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :clients, only: [:index, :create, :show]
  resources :shippings, only: [:index, :show, :create, :update]
  resources :paths, only: [:create, :update]
end
