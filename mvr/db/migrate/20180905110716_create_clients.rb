class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string :name, null: false
      t.string :lastname, null: false
      t.string :ci, null: false
      t.string :mobile, null: false
      t.string :phone

      t.timestamps
    end
  end
end
