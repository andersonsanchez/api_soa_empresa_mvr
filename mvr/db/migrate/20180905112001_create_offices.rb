class CreateOffices < ActiveRecord::Migration[5.1]
  def change
    create_table :offices do |t|
      t.string :name, null: false
      t.string :phone, null: false
      t.text :address, null: false
      t.references :state, foreign_key: {to_table: :locations}, null: false
      t.references :city, foreign_key: {to_table: :locations}, null: false

      t.timestamps
    end
  end
end
