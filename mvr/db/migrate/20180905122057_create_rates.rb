class CreateRates < ActiveRecord::Migration[5.1]
  def change
    create_table :rates do |t|
      t.decimal :price, precision: 3, scale: 2, null:false
      t.references :origin, foreign_key: {to_table: :offices}, null: false
      t.references :destination, foreign_key: {to_table: :offices}, null: false

      t.timestamps
    end
  end
end
