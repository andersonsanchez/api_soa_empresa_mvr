class CreateShippings < ActiveRecord::Migration[5.1]
  def change
    create_table :shippings do |t|
      t.date :order_date, null: false
      t.decimal :base_price, precision: 3, scale: 2, null: false
      t.date :pickup_date
      t.integer :status, null: false
      t.references :sender, foreign_key: {to_table: :clients}, null: false
      t.references :receiver, foreign_key: {to_table: :clients}, null: false
      t.references :office, foreign_key: true, null: false

      t.timestamps
    end
  end
end
