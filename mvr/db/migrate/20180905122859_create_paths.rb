class CreatePaths < ActiveRecord::Migration[5.1]
  def change
    create_table :paths do |t|
      t.date :date, null: false
      t.date :arrival_date
      t.decimal :price, precision: 3, scale: 2, null: false
      t.integer :status, null: false
      t.references :office, foreign_key: true, null: false
      t.references :shipping, foreign_key: true, null: false

      t.timestamps
    end
  end
end
