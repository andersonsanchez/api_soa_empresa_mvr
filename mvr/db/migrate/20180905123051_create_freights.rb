class CreateFreights < ActiveRecord::Migration[5.1]
  def change
    create_table :freights do |t|
      t.integer :kind, null: false
      t.decimal :weight, precision: 3, scale: 2, null: false
      t.text :description, null: false
      t.references :shipping, foreign_key: true, null: false

      t.timestamps
    end
  end
end
