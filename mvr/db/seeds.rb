# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

locations_list = [
    ["Amazonas", 0], ["Anzoátegui", 0], ["Apure", 0], ["Aragua", 0], ["Barinas", 0],
    ["Bolívar", 0], ["Carabobo", 0], ["Cojedes", 0], ["Delta Amacuro", 0], ["Distrito Capital", 0],
    ["Falcón", 0], ["Guárico", 0], ["Lara", 0], ["Mérida", 0], ["Miranda", 0],
    ["Monagas", 0], ["Nueva Esparta", 0], ["Portuguesa", 0], ["Sucre", 0], ["Táchira", 0],
    ["Trujillo", 0], ["Vargas", 0], ["Yaracuy", 0], ["Zulia", 0], ["Puerto Ayacucho", 1],
    ["Barcelona", 1], ["San Fernando de Apure", 1], ["Maracay", 1], ["Barinas", 1], ["Ciudad Bolívar", 1],
    ["Valencia", 1], ["San Carlos", 1], ["Tucupita", 1], ["Caracas", 1], ["Coro", 1],
    ["San Juan de Los Morros", 1], ["Barquisimeto", 1], ["Mérida", 1], ["Los Teques", 1], ["Maturín", 1],
    ["La Asunción", 1], ["Guanare", 1], ["Cumaná", 1], ["San Cristóbal", 1], ["Trujillo", 1],
    ["La Guaira", 1], ["San Felipe", 1], ["Maracaibo", 1]
  ]
  
  locations_list.each do |name, kind|
    Location.create( name: name, kind: kind )
  end

  offices_list = [
      ["PUERTO AYACUCHO", "0248-521.42.17", "CALLE PRINCIPAL DE LA URBANIZACION ANDRES ELOY BLANCO", 1, 25],
      ["NUEVA BARCELONA", "0281-275.48.18", "AV. FUERZAS ARMADAS, C.C. LOS CHAGUARAMOS",  2, 26],
      ["SAN FERNANDO", "0412-4014394", "CALLE INDEPENDENCIA CRUZE CON CALLE DIANA", 3, 27],
      ["TURMERO ZONA INDUSTRIAL", "0424-370-04-64 ", "AV INTERCOMUNAL MARACAY", 4, 28],
      ["BARINAS SABANETA", "0273-775.61.75", "CALLE 1 ENTRE AV. OBISPOS Y AV. ANTONIO MARIA", 5, 29],
      ["MESA LA FUENTE", "0285-632.00.18", "E/S LA FUENTE. AV.GERMANIA", 6, 30],
      ["PARAPARAL", "0241-619.14.01", "AV. PPAL DE PARAPARAL, C.C. CRISTAL", 7, 31],
      ["TINAQUILLO", " 	0258-766.04.41", "AV CARABOBO ENTRE CALLE URDANETA Y CALLE CEDEÑO", 8, 32],
      ["TUCUPITA", "0287-721.44.66 ", "CALLE DALLA COSTA # 22, LOCAL 1", 9, 33],
      ["LOS CAOBOS", "0212-781.40.19", "AV PANAMÁ CON AV LIBERTADOR", 10, 34],
      ["PUNTO FIJO CENTRO", "0269-248-37-24", "AV. BRASIL CON CALLE ZAMORA", 11, 35],
      ["EL SOMBRERO", "0246-415.97.28 ", "AV RAFAEL CALDERA", 12, 36],
      ["BARQUISIMETO ARKA", "0414-439-16-14", "CALLE 21 ENTRE CARRERA 29 Y 30", 13, 37],
      ["MERIDA MILLA", "0274-252.27.94", "AV. 5 ZERPA CON CALLE 15", 14, 38],
      ["EL LLANITO", " 	04242254861", "AV. TAMANACO, C/C TIUNA, QTA. TERESITA", 15, 39],
      ["MATURIN LA FLORESTA", "0291-641-50-77", "CR 1 Nº 4 C.C LA REDOMA NIVEL PB LOCAL 5", 16, 40],
      ["NUEVA CADIZ", "0295-274.57.05", "AVENIDA JUAN BAUTISTA ARISMEND", 17, 41],
      ["ACARIGUA", "0255-623.62.96", "CALLE 22. ENTRE AVENIDAS LIBERTADOR Y ALIANZA", 18, 42],
      ["CUMANA EL BOSQUE", "0293-414.89.24", "AV. CARUPANO, C.C. EL BOSQUE", 19, 43],
      ["UREÑA", "0276-787.29.18", "CALLE 5 CON ESQUINA CARRERA 6", 20, 44],
      ["VALERA", "0271-221.30.24", "CALLE 5 ENTRE AV. BOLIVAR Y 9. EDIF. DON PEPE", 21, 45],
      ["LA GUAIRA", "0212-352.67.35", "CALLE SILENCIO A JEFATURA LOCAL NRO 2", 22, 46],
      ["SAN FELIPE", "0416-121-6089", "2º AV. CON CALLE 16 EDIF. ROSPIER", 23, 47],
      ["CIUDAD OJEDA CENTRO", "0265-631.60.49", "CALLE FARIA ESQ. CALLE LARA CC TED", 24, 48],
  ]

  offices_list.each do |name, phone, address, state_id, city_id|
    Office.create( name: name, phone: phone, address: address, state_id: state_id, city_id: city_id )
  end

  Rate.create( price: 123.25, origin_id: 1, destination_id: 4)
  Rate.create( price: 456.13, origin_id: 4, destination_id: 2)
  Rate.create( price: 789.56, origin_id: 6, destination_id: 8)
  Rate.create( price: 987.78, origin_id: 10, destination_id:11)
  Rate.create( price: 654.31, origin_id: 24, destination_id: 19)

  Client.create( name: "prueba", lastname: "prueba2", ci: "372832", mobile: "2323232", phone: "2323232")