
module CreateShippingFreightPath

    def create_all_three

    @shipping = Shipping.new
    @shipping.status = "pending"
    @shipping.order_date = DateTime.now
    @shipping.base_price = shipping_params[:base_price]
    @sender = Client.find shipping_params[:sender_id]
    @shipping.sender = @sender
    @receiver = Client.find shipping_params[:receiver_id]
    @shipping.receiver = @receiver
    @office = Office.find shipping_params[:office_id]
    @shipping.office = @office
    @shipping.save

    @shipping_id = Shipping.last.id

    @freight = Freight.new
    @freight.kind = "document"
    @freight.weight = "333.98"
    @freight.description = "pieza de computadora"
    @freight.shipping_id = @shipping_id
    @freight.save

    @path = Path.new
    @path.date = DateTime.now
    @path.arrival_date = DateTime.now
    @path.price = "998.02"
    @path.status = "current"
    @path.office_id = 2
    @path.shipping_id = @shipping_id
    @path.save

    render json: @shipping
    end

end
